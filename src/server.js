const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');
const app = express();

mongoose.connect('mongodb://localhost/colomboBD', {useNewUrlParser: true })
    .then(db => console.log('DB está conectada'))
    .catch(err => console.log(err))


//configuraciones
app.set('port', process.env.PORT || 3000)

//middlewares (funciones antes de que entren a las rutas)
app.use(morgan('dev'));
app.use(express.json());

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');
       next();
 });

//Rutas
app.use('/ganado', require('./routes/ganados'));


app.listen(app.get('port'), () => {
    console.log("servidor en el puerto", app.get('port'))
})

