const mongoose = require('mongoose');
const { Schema } = mongoose;

const Ganado = new Schema ({
    tipo: String
});

module.exports = mongoose.model('Ganado', Ganado);