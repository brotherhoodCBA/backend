const express = require('express');
const router = express.Router();

const Ganado = require('../models/Ganado')

// Get all ganados (TIPO DE GANADO)
router.get('/', async (req, res) => {
    const ganados = await Ganado.find()
    res.json(ganados)
})


// Get an especific GANADO
router.get('/:_id', async(req, res) => {
    const task = await Ganado.findById(req.params._id)
    res.send(task)
})

// Save a new ganado
router.post('/', async (req, res) => {
    const task = new Ganado(req.body)
    await task.save()
    res.json({
        estado: 'Tipo de Ganado guardado!'
    })
})

// Update a Ganado
router.put('/:_id', async (req, res) => {
    await Ganado.findByIdAndUpdate(req.params._id, req.body)
    res.json({
        estado: 'Tipo de Ganado actualizado!'
    })
})

// Delete a task
router.delete('/:_id', async (req, res) => {
    await Ganado.findByIdAndDelete(req.params._id)
    res.json({
        estado: "Tipo de Ganado eliminado!"
    })

})

// Delete all
router.delete('/', async (req, res) => {
    await Ganado.deleteMany()
    res.json({
        estado: "Ganados eliminadas!"
    })

})


module.exports = router;
